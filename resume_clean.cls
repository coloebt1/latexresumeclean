%=============================================================================================
%=============================================================================================
%	resume_clean.cls
%
%	Author: 	Brandon Coloe
%
%	Description: 
%		
%
%
%	Known Limitations:
%		+ 
%
%=============================================================================================
%=============================================================================================

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{resume_clean}[2018/09/12 Clean resume class]

\LoadClass[10pt]{article}
\RequirePackage[letterpaper,left=1in,right=1in,top=0.751in,bottom=0.75in]{geometry}

\RequirePackage[scaled]{helvet}
\RequirePackage{scrextend}
\RequirePackage{enumitem}
\RequirePackage{amsmath}
\RequirePackage{bibentry}
\RequirePackage{tabularx}
\RequirePackage[none]{hyphenat}
\RequirePackage{etoolbox}
\RequirePackage{forloop}
\RequirePackage{fmtcount}

\pagenumbering{gobble}

\renewcommand\familydefault{\sfdefault} 
\RequirePackage[T1]{fontenc}

\setlength{\parindent}{0pt}
\setlength{\floatsep}{-1cm}
\righthyphenmin=62
\lefthyphenmin=62
\sloppy

\def\secspace{0.5em}
\def\educationspace{0.5em}
\def\jobspace{0.5em}


%-----------------------------------------------------------------------------------------------
% Make Document Tittle
%-----------------------------------------------------------------------------------------------

%Title Line Variables
\def\address#1{\gdef\@address{#1}}					%address (street, city, state, zip code)
\def\phone#1{\gdef\@phone{#1}}					%phone number
\def\email#1{\gdef\@email{#1}}						%email address

%Title Making Command
\renewcommand{\maketitle}{%
	\begin{minipage}[t]{0.48\textwidth}
		\vspace*{\fill}
		{\huge \textsc{\textbf{\@author}}\par}
		\vspace*{\fill}
	\end{minipage}
	\begin{minipage}[t]{0.52\textwidth}
		\begin{flushright}
		\@address\\
		 \@phone\\
		 \texttt{\@email}
	 	\end{flushright}
	\end{minipage}
}

%-----------------------------------------------------------------------------------------------
% Objective Statement Section
%-----------------------------------------------------------------------------------------------
\def\objective#1{\gdef\@objective{#1}}

\newcommand{\makeObjectiveStatement}{%
	%make horizontal line
	\makeSectionSep

	{\large\textbf{\textsc{Objective}}\par}
	\vspace{\secspace}
	
	\begin{addmargin}[1em]{0em}
		\@objective
	\end{addmargin}
}

%-----------------------------------------------------------------------------------------------
% Summary Statement Section
%-----------------------------------------------------------------------------------------------
\def\summary#1{\gdef\@summary{#1}}

\newcommand{\makeSummary}{%
	%make horizontal line
	\makeSectionSep

	{\large\textbf{\textsc{Summary}}\par}
	\vspace{\secspace}
	
	\begin{addmargin}[1em]{0em}
		\@summary
	\end{addmargin}
}

%-----------------------------------------------------------------------------------------------
% Education Section
%-----------------------------------------------------------------------------------------------

%Education Variables
\newcounter{SchoolNumber}

\newcommand\addschool[1]{
	\stepcounter{SchoolNumber}
	\csdef{School\theSchoolNumber}{#1}
	\csdef{Degree\theSchoolNumber}{}
	\csdef{GradDate\theSchoolNumber}{}
	\csdef{GPA\theSchoolNumber}{}
	\csdef{SchoolNote\theSchoolNumber}{}
}

\newcommand\setSchool[2]{%
	\csdef{School#1}{#2}%
}

\newcommand\getSchool[1]{%
	\csuse{School#1}%
}

\newcommand\setDegree[2]{%
	\csdef{Degree#1}{#2}%
}

\newcommand\getDegree[1]{%
	\csuse{Degree#1}%
}

\newcommand\setGradDate[2]{%
	\csdef{GradDate#1}{#2}%
}

\newcommand\getGradDate[1]{%
	\csuse{GradDate#1}%
}

\newcommand\setGPA[2]{%
	\csdef{GPA#1}{#2}%
}

\newcommand\getGPA[1]{%
	\csuse{GPA#1}%
}

\newcommand\getSchoolNote[1]{%
	\csuse{SchoolNote#1}%
}

\newcommand\setSchoolNote[2]{%
	\csdef{SchoolNote#1}{#2}%
}

\def\relevantCoursework#1{\gdef \@relevantCoursework{#1}}

\newcommand\makeEducation[0]{

	%make horizontal line
	\makeSectionSep

	{\large\textbf{\textsc{Education}}\par}
	\vspace{\secspace}

	%iterator variable
	\newcounter{sn}
	
	%number of schools incremented by one
	\newcounter{SchoolNumberPlusOne}
	\setcounter{SchoolNumberPlusOne}{\theSchoolNumber}
	\stepcounter{SchoolNumberPlusOne}
	
	
	\begin{addmargin}[1em]{0em}
	
	\forloop[1]{sn}{1}{\value{sn} < \value{SchoolNumberPlusOne}}{
		\NOSECONDLINEtrue
		\textbf{\getDegree{\thesn}}, \getSchool{\thesn} \hfill \getGradDate{\thesn}%
		\makeSchoolNote{\thesn}
		\makeGPA{\thesn}
		
		\ifnum\value{sn}=\value{SchoolNumber}
		\else
			\vspace{\educationspace}
		\fi
	}%
	%Add Relevant Coursework Section
	\ifx \@relevantCoursework\undefined
	\else	
		\vspace{\educationspace}
		\makeatletter
		\emph{Relevant Coursework:} \@relevantCoursework
		\makeatother
	\fi
	\end{addmargin}
}

\newif\ifNOSECONDLINE

%make school note
\newcommand\makeSchoolNote[1]{%
	\ifcsstring{SchoolNote#1}{}
	{%
	}%
	{%
		\NOSECONDLINEfalse
		\\%
		\getSchoolNote{\thesn}%
	}%
}%

%make GPA line
\newcommand\makeGPA[1]{%
	\ifcsstring{GPA#1}{}
	{%
	}%
	{%
		\ifNOSECONDLINE
			\\%
		\fi
		\null\hfill GPA: \getGPA{#1}%
	}%
}%

\newcommand\makeSectionSep[0]{
	\noindent\makebox[\linewidth]{\rule{\textwidth}{0.4pt}}
}


%-----------------------------------------------------------------------------------------------
% Work Experience Section
%-----------------------------------------------------------------------------------------------

%number of jobs to be included
\newcounter{JobNumber}
%\newcounter{ExperienceCounter}

%Macro: 	localexperiencename
%Brief:	Defines a variable named 'Experience<job#>_<exp#>' for later use in generating work 
%		experience section
%Usage: 	localexperiencename{ experience_string }
\makeatletter
\def\localexperiencename#1{%
  \@namedef{Experience\theJobNumber_\getExperienceCounterValue{\theJobNumber}}{#1}
}

%Macro: 	getexperience
%Brief: 	Return the job experience string associated with 'Experience<job#>_<exp#>
%Usage: 	getexperience{ job#, experience# }
\newcommand{\getexperience}[2]{%
  \@nameuse{Experience#1_#2}%
}
\makeatother

%Macro: 	nExperiences
%Brief: 	Store the number of experiences for the current job number
%Usage: 	nExperiences{ experience# }
\makeatletter
\def\nExperiences#1{%
\protected@edef\@tempa{\theJobNumber}%
\protected@edef\@tempb{#1}%
 \@namedef{nExp\@tempa}{\@tempb}
 }
 
 %Macro: 	getnexp
 %Brief: 	Get the total number of experiences associated with a given job number
 \newcommand{\getnexp}[1]{%
 \arabic{ExperienceCounter#1}%
 }
 \makeatother


%Not really sure that collect or generateOutput are used
%\newcommand{\collect}{}
%
%	\makeatletter
%\newcommand{\addstuff}[2]{%
%  \protected@edef\@tempa{#1}%
%  \expandafter\g@addto@macro\expandafter\collect\expandafter{%
%    \@tempa & #2 \\}}
%\makeatother
%
%\newcommand{\generateOutput}{%
%    Output of collection\par%
%    \begin{tabular}{rl}\collect\end{tabular}%
%}

%Macro: 	addjob
%Brief: 	Add a new job to the resume
%Usage: 	addjob{ jobTitle, employmentDates, EmployerName, EmployerLocation}
\newcommand\addjob[4]{
	\stepcounter{JobNumber}
	\csdef{JobTitle\theJobNumber}{#1}
	\csdef{EmploymentDates\theJobNumber}{#2}
	\csdef{Employer\theJobNumber}{#3}
	\csdef{EmployerLocation\theJobNumber}{#4}
}

%Macro:	getJobTitle
%Brief: 	Return job title associated with given job number
%Usage: 	getJobTitle{ jobNumber }
\newcommand\getJobTitle[1]{%
	\csuse{JobTitle#1}%
}

%Macro: 	getNewExperienceCounter
%Brief: 	Create a new counter variable to keep track of a job's experiences
%Usage: 	getNewExperienceCounter{ jobNumber }
\newcommand\getNewExperienceCounter[1]{%
	\newcounter{ExperienceCounter#1}%
	\setcounter{ExperienceCounter#1}{0}%
}

%Macro: 	getExperienceCounterValue
%Brief: 	Get the value of the experience counter for a give job
%Usage: 	getExperienceCounterValue{ jobNumber }
\newcommand\getExperienceCounterValue[1]{%
	\arabic{ExperienceCounter#1}%
}

%Macro: 	getEmploymentDates
%Brief: 	Get the dates of employment associated with a given job 
%Usage: 	getEmploymentDates{ jobNumber }
\newcommand\getEmploymentDates[1]{%
	\csuse{EmploymentDates#1}%
}

%Macro: 	getEmployer
%Brief: 	Get the employer name associated with a given job
%Usage: 	getEmployer{ jobNumber }
\newcommand\getEmployer[1]{%
	\csuse{Employer#1}%
}

%Macro: 	getEmployerLocation
%Brief: 	Get the employer location associated with a given job
%Usage: 	getEmployerLocation{ jobNumber }
\newcommand\getEmployerLocation[1]{%
	\csuse{EmployerLocation#1}%
}

%Macro: 	ifcounter
%Brief: 	Conditional operation if the specified counter is defined
%Usage: 	ifcounter{ counterName }{ doIfExists }{ doIfDoesNotExist }
\makeatletter
\newcommand*\ifcounter[1]{%
  \ifcsname c@#1\endcsname
    \expandafter\@firstoftwo
  \else
    \expandafter\@secondoftwo
  \fi
}
\makeatother

%Macro: 	addExperience
%Brief: 	Add an experience entry for the current job number
%Usage: 	addExperience{ experienceStr }
\newcommand\addExperience[1]{
	\ifcounter{ExperienceCounter\theJobNumber}{}{\getNewExperienceCounter{\theJobNumber}}
	\stepcounter{ExperienceCounter\theJobNumber}
	\nExperiences{\arabic{ExperienceCounter\theJobNumber}}
	\makeatletter
	\localexperiencename{#1}
	\makeatother
}

%Macro: 	makeWorkExperience
%Brief: 	Make the work experience section of the resume
%Usage: 	makeWorkExperience
\newcommand\makeWorkExperience[0]{

	%make horizontal line
	\makeSectionSep

	{\large\textbf{\textsc{Work Experience}}\par}
	\vspace{\secspace}
	
	%
	\newcounter{jn}
	\newcounter{en}
	
	%number of jobs incremented by one
	\newcounter{JobNumberPlusOne}
	\setcounter{JobNumberPlusOne}{\theJobNumber}
	\stepcounter{JobNumberPlusOne}
	
	\newcounter{JobExperienceCounterPlusOne}
	
	\begin{addmargin}[1em]{0em}
	
	\forloop[1]{jn}{1}{\value{jn} < \value{JobNumberPlusOne}}{
		\textbf{\getJobTitle{\thejn}} \hfill \getEmploymentDates{\thejn}\\
		\emph{\getEmployer{\thejn} \hfill \getEmployerLocation{\thejn}}%
		%
		\setcounter{JobExperienceCounterPlusOne}{\getnexp{\thejn}}%
		\stepcounter{JobExperienceCounterPlusOne}%
		%
		\ifnum\value{JobExperienceCounterPlusOne}=1
		\else
			\begin{itemize}[itemsep=-0.2em,topsep=2pt]
			\forloop[1]{en}{1}{\value{en} < \value{JobExperienceCounterPlusOne}}{%
				\item \getexperience{\thejn}{\theen}%
			}
			\end{itemize}%
		\fi
		\ifnum\value{jn}=\value{JobNumber}
		\else
			\vspace{\jobspace}
		\fi
	}
	\end{addmargin}

}

%-----------------------------------------------------------------------------------------------
% Skills and Awards
%-----------------------------------------------------------------------------------------------


%-----------------------------------------------------------------------------------------------
% Publications
%-----------------------------------------------------------------------------------------------

%-----------------------------------------------------------------------------------------------
% Custom Section
%-----------------------------------------------------------------------------------------------
\newenvironment{newSec}[1]
{
	%make horizontal line
	\makeSectionSep
	{\large\textbf{\textsc{#1}}\par}
	\vspace{\secspace}
	\begin{addmargin}[1em]{0em}
}
{
	\end{addmargin}
}






